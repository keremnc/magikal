package net.frozenorb.magikal.bossbar;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;

import net.frozenorb.magikal.Magikal;
import net.frozenorb.magikal.Profile;
import net.minecraft.server.v1_7_R1.DataWatcher;
import net.minecraft.server.v1_7_R1.Entity;
import net.minecraft.server.v1_7_R1.EntityEnderDragon;
import net.minecraft.server.v1_7_R1.Packet;
import net.minecraft.server.v1_7_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_7_R1.PacketPlayOutSpawnEntityLiving;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_7_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_7_R1.entity.CraftPlayer;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class ManaUpdateTask extends BukkitRunnable {
	private static final int ENTITY_ID_MODIFIER = 1236912369;
	private static final int PASSES_PER_FLASH = 16;

	private HashSet<String> playersToUpdate = new HashSet<String>();
	private HashMap<String, FlashMessage> flashMessages = new HashMap<String, FlashMessage>();

	
	@Override
	public void run() 
	{
		for (Profile p : Magikal.getInstance().getMagikalListener().getAllProfiles().values()) 
		{
			setBar(p);

		}
	}

	public void addToUpdateQueue(Player p) {
		playersToUpdate.add(p.getName());
	}

	public void setBar(final Profile profile) 
	{
		final Player p = profile.getPlayer();
		long has = profile.getMana();
		String display = "";
		int needed = profile.getMaxMana();
		double percent = ((double) has / (double) needed) * 100D;
		percent = Math.floor(percent * 10.0) / 10.0;
		display = "§9 Mana§e  -  " + has + "/" + needed + " §bMP§e [" + percent + "%]";
		if (flashMessages.containsKey(p.getName())) 
		{
			if (flashMessages.get(p.getName()).timesDone >= PASSES_PER_FLASH) 
			{
				flashMessages.remove(p.getName());
			}
			if (flashMessages.containsKey(p.getName())) 
			{
				display += " " + flashMessages.get(p.getName()).message;
				flashMessages.get(p.getName()).timesDone++;
			}
		}
		float health = (float) (((double) has / (double) needed) * 200D);
		spawnBar(p, display, health);
		Bukkit.getScheduler().runTaskLater(Magikal.getInstance(), new Runnable() 
		{

			@Override
			public void run() 
			{
				removePlayer(p);

			}
		}, 3);

	}

	public void flashMessage(Player p, String message) {
		flashMessages.put(p.getName(), new FlashMessage(message));
	}

	public void removePlayer(Player player) 
	{
		PacketPlayOutEntityDestroy pac = new PacketPlayOutEntityDestroy(player.getEntityId() + ENTITY_ID_MODIFIER);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(pac);
	}

	/*
	 * ------------PRIVATE PACKET METHODS---------------
	 */
	private void spawnBar(Player player, String display, float health) {
		displayTextBar(display, player, health);
	}

	private void sendPacket(Player player, Packet packet) {
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
	}

	@SuppressWarnings("deprecation")
	private PacketPlayOutSpawnEntityLiving constrcuctPacket(Player p, String text, Location loc, final float health) 
	{
		PacketPlayOutSpawnEntityLiving mobPacket = new PacketPlayOutSpawnEntityLiving();
		final EntityEnderDragon dragon = new EntityEnderDragon(((CraftWorld) p.getWorld()).getHandle());
		
		dragon.setSneaking(true);
		dragon.setInvisible(true);
		dragon.setHealth(health);
		
		int x = (int) Math.floor(loc.getBlockX() * 32.0D);
		int y = (int) Math.floor(-10);
		int z = (int) Math.floor(loc.getBlockZ() * 32.0D);
		try 
		{
			/* id */
			Field cID = mobPacket.getClass().getDeclaredField("a");
			cID.setAccessible(true);
			cID.set(mobPacket, (int) p.getEntityId() + ENTITY_ID_MODIFIER);
			cID.setAccessible(false);
			/* name */
			Field cName = mobPacket.getClass().getDeclaredField("b");
			cName.setAccessible(true);
			cName.set(mobPacket, EntityType.ENDER_DRAGON.getTypeId());
			cName.setAccessible(false);
			/* x */
			Field cF = mobPacket.getClass().getDeclaredField("c");
			cF.setAccessible(true);
			cF.set(mobPacket, x);
			cF.setAccessible(false);
			/* y */
			Field cY = mobPacket.getClass().getDeclaredField("d");
			cY.setAccessible(true);
			cY.set(mobPacket, y);
			cY.setAccessible(false);
			/* z */
			Field cZ = mobPacket.getClass().getDeclaredField("e");
			cZ.setAccessible(true);
			cZ.set(mobPacket, z);
			cZ.setAccessible(false);
		} 
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		DataWatcher watcher = getWatcher(text, dragon);
		watcher.a(6, (Float) (float) health);
		try 
		{
			Field t = PacketPlayOutSpawnEntityLiving.class.getDeclaredField("l");
			t.setAccessible(true);
			t.set(mobPacket, watcher);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return mobPacket;
	}

	private DataWatcher getWatcher(String text, Entity e) 
	{
		DataWatcher watcher = new DataWatcher(null);
		watcher.a(0, (Byte) (byte) 0x20);
		watcher.a(10, (String) text);
		watcher.a(11, (Byte) (byte) 1);
		return watcher;
	}

	private void displayTextBar(String text, final Player player, float health) 
	{
		PacketPlayOutSpawnEntityLiving mobPacket = constrcuctPacket(player, text, player.getLocation(), health);
		sendPacket(player, mobPacket);
	}

	public class FlashMessage 
	{
		int timesDone;
		String message;

		public FlashMessage(String message) 
		{
			this.message = message;
		}
	}
}

package net.frozenorb.magikal.spell;

import org.bukkit.ChatColor;

public enum SpellSlot {

	FIRST(0),
	
	SECOND(1),
	
	THIRD(2),
	
	LAST(3);
	
	private int slot;
	public int getSlot() { return slot; }
	
	private SpellSlot(int slot) 
	{
		this.slot = slot;
	}
	
	public static SpellSlot parse(String name)
	{
		
		for (SpellSlot ss : values())
		{
			
			if (ChatColor.stripColor(ss.toString()).equalsIgnoreCase(ChatColor.stripColor(name)))
			{
				
				return ss;
			}
		}
		
		return null;
	}
	
}

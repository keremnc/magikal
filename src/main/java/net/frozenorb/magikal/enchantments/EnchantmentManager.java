package net.frozenorb.magikal.enchantments;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.enchantments.Enchantment;

@SuppressWarnings("deprecation")
public class EnchantmentManager {

	private static final int[] BVAL = { 1000, 900, 500, 400, 100, 90, 50, 40,
			10, 9, 5, 4, 1 };
	private static List<Integer> customEnchants = new ArrayList<Integer>();

	private static final String[] RCODE = { "M", "CM", "D", "CD", "C", "XC",
			"L", "XL", "X", "IX", "V", "IV", "I" };

	public static Enchantment INVISIBLE;

	static 
	{
		if (Bukkit.getOnlinePlayers().length == 0)
		{
			INVISIBLE = new Invisible(getId());

			try 
			{
				Field field = Enchantment.class.getDeclaredField("acceptingNew");	
				field.setAccessible(true);
				field.setBoolean(EnchantmentManager.INVISIBLE, true);
			} 
			catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) 
			{
				e.printStackTrace();
			}
	
			if (Enchantment.getById(INVISIBLE.getId()) == null) {
				Enchantment.registerEnchantment(INVISIBLE);
			}
		}
	}

	private static int getId() 
	{
		for (int i = 1; i <= 1000; i++) 
		{
			if (Enchantment.getById(i) == null && !customEnchants.contains(i)) 
			{
				customEnchants.add(i);
				return i;
			}
		}
		return 0;
	}

	public static boolean isNatural(Enchantment ench) {
		
		return customEnchants.contains(ench.getId());
	}

	public static String toRoman(int binary) 
	{
		if (binary <= 0) return "";
		
		String roman = "";
		for (int i = 0; i < RCODE.length; i++) 
		{
			while (binary >= BVAL[i]) 
			{
				binary -= BVAL[i];
				roman += RCODE[i];
			}
		}
		return roman;
	}

}
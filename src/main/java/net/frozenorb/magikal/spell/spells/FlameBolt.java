package net.frozenorb.magikal.spell.spells;

import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.FallingBlock;

import net.frozenorb.magikal.Profile;
import net.frozenorb.magikal.spell.Spell;
import net.frozenorb.magikal.spell.SpellSlot;

public class FlameBolt extends Spell {

	private Random r = new Random();
	
	@Override
	public boolean onActivate(Profile profile) {
		Location loc = profile.getPlayer().getLocation();

		for (int i = 0; i < 5; i ++)
		{
			FallingBlock fb = loc.getWorld().spawnFallingBlock(loc.clone().add(r.nextInt(2) - 1, r.nextInt(2) - 1, r.nextInt(2) - 1), Material.FIRE, (byte) 0);
			fb.setVelocity(loc.getDirection().multiply(5));
			fb.setDropItem(false);
			fb.setFireTicks(1000);
			
		}
		return true;
	
	}
	

	@Override
	public double getCooldown() {
		return 4;
	}

	@Override
	public int getManaCost() {
		return 20;
	}

	@Override
	public Material getMaterial() {
		return Material.FIRE;
	}

	@Override
	public String getDescription() {
		return "Launches a volley of flying fireballs in the direction you are facing.";
	}

	@Override
	public String getDisplayName() {
		return "Flame Bolt";
	}

	@Override
	public SpellSlot getSlot() {
		return SpellSlot.FIRST;
	}

}

package net.frozenorb.magikal.spell.spells;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import net.frozenorb.magikal.Magikal;
import net.frozenorb.magikal.Profile;
import net.frozenorb.magikal.spell.Spell;
import net.frozenorb.magikal.spell.SpellSlot;

public class IceSpikes extends Spell {
	private static final BlockFace[] AXIAL = new BlockFace[] {
		BlockFace.NORTH, BlockFace.EAST, BlockFace.WEST, BlockFace.SOUTH
	};
	
	private static final BlockFace[] RADIAL = new BlockFace[] {
		BlockFace.NORTH_EAST, BlockFace.SOUTH_EAST, BlockFace.NORTH_WEST, BlockFace.SOUTH_WEST
	};
	
	private Random r = new Random();
	
	@Override
	public boolean onActivate(Profile profile) 
	{
		@SuppressWarnings("deprecation")
		Block loc = profile.getPlayer().getTargetBlock(null, 9);
		int iterations = 0;
		
		while (iterations < 11 && !loc.getType().isSolid())
		{
			iterations++;
			loc = loc.getRelative(BlockFace.DOWN);
			
		}

		for (int i = 0; i < 5; i ++)
		{
			Location spawnAt = loc.getLocation().clone().add(r.nextInt(10) - 5, r.nextInt(10) - 5, r.nextInt(10) - 5);
			int chosen = 15 + r.nextInt(7);
			createTempPillar(spawnAt, getMaterial(), chosen, 15);
		}
		return true;
	
	}
	
	private void damageNearby(Location loc)
	{
		int radius = 6;
		int chunkRadius = radius < 16 ? 1 : (radius - (radius % 16)) / 16;
		for (int chX = 0 -chunkRadius; chX <= chunkRadius; chX ++)
		{
			
			for (int chZ = 0 -chunkRadius; chZ <= chunkRadius; chZ++)
			{
				
				int x = (int) loc.getX(), y = (int) loc.getY(), z = (int) loc.getZ();
				for (Entity e : new Location(loc.getWorld(), x + (chX * 16), y, z + (chZ * 16)).getChunk().getEntities())
				{
					if (e.getLocation().distance(loc) <= radius && e.getLocation().getBlock() != loc.getBlock())
					{
						if (e instanceof Player)
						{
							Player p = (Player) e;
							p.damage(4D);
						}
					}
				}
			}
		}	
	}
	private void createTempPillar(Location loc, Material mat, int height, int seconds)
	{
		final Location bsloc = loc.clone();
		final ArrayList<Block> changed = new ArrayList<Block>();
		
		for (int i = 0; i < height; i ++)
		{
			
			Block b = loc.add(0, 1, 0).getBlock();
			if (b.getType() == Material.AIR)
			{
				b.setType(mat);			
				b.getWorld().playEffect(b.getLocation(), Effect.STEP_SOUND, b.getType());
				changed.add(b);
				damageNearby(b.getLocation());
			}
		}	
		for (BlockFace rad: AXIAL)
		{
			Location base = bsloc.getBlock().getRelative(rad).getLocation();
			for (int i = 0; i < height - 3; i ++)
			{
			
				Block b = base.add(0, 1, 0).getBlock();
				if (b.getType() == Material.AIR)
				{
					b.setType(mat);
					b.getWorld().playEffect(b.getLocation(), Effect.STEP_SOUND, b.getType());
					changed.add(b);
					damageNearby(b.getLocation());

				}
			
			}
		}
		for (BlockFace rad: RADIAL)
		{
			Location base = bsloc.getBlock().getRelative(rad).getLocation();
			for (int i = 0; i < height - 6; i ++)
			{
			
				Block b = base.add(0, 1, 0).getBlock();
				if (b.getType() == Material.AIR)
				{
					changed.add(b);
					b.getWorld().playEffect(b.getLocation(), Effect.STEP_SOUND, b.getType());
					b.setType(mat);
					damageNearby(b.getLocation());

				}			
			}
		}
		Bukkit.getScheduler().runTaskLater(Magikal.getInstance(), new Runnable()
		{
			@Override
			public void run() 
			{
				for (Block b : changed)
				{
					b.setType(Material.AIR);
				}
			}
		}, seconds * 20);
	}
	

	@Override
	public double getCooldown() {
		return 6;
	}

	@Override
	public int getManaCost() {
		return 50;
	}

	@Override
	public Material getMaterial() {
		return Material.PACKED_ICE;
	}

	@Override
	public String getDescription() {
		return "Call up giant spikes of ice to come from the ground below and damage your enemies.";
	}

	@Override
	public String getDisplayName() {
		return "Ice Spikes";
	}

	@Override
	public SpellSlot getSlot() {
		return SpellSlot.SECOND;
	}

}

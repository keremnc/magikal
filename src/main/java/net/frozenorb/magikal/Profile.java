package net.frozenorb.magikal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.frozenorb.magikal.spell.Spell;
import net.frozenorb.magikal.spell.SpellSlot;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Profile {

	private String playerName;
	public String getPlayerName() { return playerName; }

	private HashMap<SpellSlot, Spell> spellsInUse = new HashMap<SpellSlot, Spell>();
	public HashMap<SpellSlot, Spell> getSpellsInUse() {	return spellsInUse; }

	private SpellSlot currentSlot;
	public SpellSlot getCurrentSlot() { return currentSlot; }
	public void setCurrentSlot(SpellSlot slot) { this.currentSlot = slot; }

	private int mana;
	public int getMana() { return mana; }
	public void setMana(int mana) { this.mana = mana; }
	
	private int maxMana = 100;
	public int getMaxMana() { return maxMana; }
	public void setMaxMana(int maxMana) { this.maxMana = maxMana; }
	
	public Profile(String playerName) {
		this.playerName = playerName;
	}
	
	public Player getPlayer() {
		return Bukkit.getPlayerExact(playerName);
	}

	public void sendMessage(String message) {
		getPlayer().sendMessage(message);
	}
	
	public boolean isUsingSpell(Spell s) {
		return spellsInUse.values().contains(s);
	}

	public void setSpell(SpellSlot slot, Spell spell) {
		spellsInUse.put(slot, spell);
	}
	
	public void updateSpellInventory() 
	{
		for (SpellSlot ss : SpellSlot.values()) {
			if (spellsInUse.get(ss) != null) {
				
				getPlayer().getInventory().setItem(ss.getSlot(), spellsInUse.get(ss).createIcon(this));
			}
		}
	}

	/**
	 * Opens a specific slot's inventory
	 * 
	 * @param slot
	 *            the slot of the inventory to open
	 */
	public void openSpellInventory(SpellSlot slot) 
	{
		Inventory inv = Bukkit.createInventory(getPlayer(), 36, centerTitle("§8Select your " + slot.toString().toLowerCase() + " spell."));

		for (Spell s : Magikal.getInstance().getMagikalListener().SPELLS) 
		{
			if (s.getSlot()
					== 
					slot)
			{

				ItemStack item = s.createIcon(this);

				if (getSpellsInUse().containsKey(s)) 
				{

					ItemMeta meta = item.getItemMeta();
					List<String> lore = meta.getLore();

					lore.add("§a§lCURRENTLY SELECTED");
					meta.setLore(lore);
				}

				inv.addItem(item);
			}
		}

		getPlayer().openInventory(inv);
	}

	/**
	 * Opens the slot selection inventory
	 */
	public void openSpellInventory() 
	{
		Inventory inv = Bukkit.createInventory(getPlayer(), 36, centerTitle("§8Select spell slot."));

		for (int i = 0; i < 36; i++) {

			inv.setItem(i, new ItemStack(Material.THIN_GLASS));
		}

		int start = 10;
		for (SpellSlot ss : SpellSlot.values()) 
		{

			ItemStack item = new ItemStack(Material.BOOK);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName("§b§l" + ss.toString());
			meta.setLore(wrap("Click here to select your " + ss.toString().toLowerCase() + " spell."));
			item.setItemMeta(meta);
			inv.setItem(start, item);
			
			start += 2;
		}
		
		getPlayer().openInventory(inv);

	}

	private String centerTitle(String title)
	{
		int titleLength = title.length();

		int spaces = 19 - (int) (titleLength / 2);

		String titleResult = "";

		for (int i = 0; i < spaces; i++) {

			if (titleResult.length() + title.length() < 32)
				titleResult += " ";
		}

		titleResult += title;

		return titleResult;
	}	
	
	public List<String> wrap(String string) 
	{
		String[] split = string.split(" ");
		string = "";
		ChatColor color = ChatColor.BLUE;
		ArrayList<String> newString = new ArrayList<String>();
		for (int i = 0; i < split.length; i++) 
		{

			if (string.length() > 20 || string.endsWith(".") || string.endsWith("!")) 
			{

				newString.add(color + string);
				if (string.endsWith(".") || string.endsWith("!")) 
				{

					newString.add("");
				}
				string = "";
			}
			string += (string.length() == 0 ? "" : " ") + split[i];
		}
		newString.add(color + string);
		
		return newString;
	}
	
	
}

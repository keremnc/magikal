package net.frozenorb.magikal;

import java.util.HashMap;
import java.util.Map;

import net.frozenorb.magikal.spell.Spell;
import net.frozenorb.magikal.spell.SpellSlot;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

public class MagikalListener implements Listener {

	public static final int DEFAULT_SLOT = 7;
	
	public final Spell[] SPELLS = new Spell[Magikal.getInstance().getSpellAmount()];	// Put a1ll available spells here
	private Map<String, Profile> profiles;
	
	public MagikalListener() {
		profiles = new HashMap<String, Profile>();
		
	}
	
	public Profile getProfile(String playerName) {
		return profiles.get(playerName);
	}
	
	public Profile getProfile(Player player) {
		return getProfile(player.getName());
	} 
	
	public Map<String, Profile> getAllProfiles() {
		return profiles;
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		profiles.put(player.getName(), new Profile(player.getName()));
	}
	
	@EventHandler
	public void onPlayerKick(PlayerKickEvent event) {
		profiles.remove(event.getPlayer().getName());
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		profiles.remove(event.getPlayer().getName());
	}
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event) {
		if (getProfile(event.getPlayer().getName()) != null)
		{
			getProfile(event.getPlayer().getName()).setCurrentSlot(null);
		}
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		ItemStack item = event.getCurrentItem();
		Profile profile = getProfile(event.getWhoClicked().getName());
		
		if (item == null) return; 
		
		if (item.getType() == Material.THIN_GLASS)
		{
			event.setCancelled(true);
		}
		
		if (item.hasItemMeta() && item.getItemMeta().getDisplayName() != null)
		{
			String name = item.getItemMeta().getDisplayName();
			
			
			if (SpellSlot.parse(name) != null) {
				event.setCancelled(true);

				profile.openSpellInventory(SpellSlot.parse(name));
				profile.setCurrentSlot(SpellSlot.parse(name));
				return;
			}
			
			if (getSpellByName(name) != null) {
				event.setCancelled(true);

				if (profile.getCurrentSlot() != null)
				{
					profile.getPlayer().playSound(profile.getPlayer().getLocation(), Sound.FIRE_IGNITE, 20, 5);
					profile.setSpell(profile.getCurrentSlot(), getSpellByName(name));
					profile.getPlayer().sendMessage(ChatColor.GREEN + "You have selected the " + getSpellByName(name).getDisplayName() + "§a spell!");
				}
			}

		}
	}
	
	@EventHandler
	public void onSpellActivate(PlayerItemHeldEvent event) {
		
		event.setCancelled(true);
		event.getPlayer().getInventory().setHeldItemSlot(DEFAULT_SLOT);
		
		int newSlot = event.getNewSlot();
		Player player = event.getPlayer();
		ItemStack wandItem = player.getInventory().getItem(newSlot);
		
		if (wandItem != null)
		{
			Spell spell = getSpell(wandItem);
			if (spell != null)
			{

				if (getProfile(player).isUsingSpell(spell))
				{

					if (spell.activate(getProfile(player))) 
					{
						spell.setLastUse(event.getPlayer().getName());
						getProfile(player).setMana(getProfile(player).getMana() - spell.getManaCost());
						Magikal.getInstance().getLevelUpdateTask().flashMessage(player, "  §b-" + spell.getManaCost() + "MP");
					}
				}
			}
		}
	}
	
	public Spell getSpell(ItemStack wandItem) {
		for (Spell spell : SPELLS)
		{
			if (spell.matches(wandItem))
			{
				return spell;
			}
		}
		
		return null;
	}
	
	public Spell getSpellByName(String name) {
		for (Spell spell : SPELLS)
		{
			
			if (ChatColor.stripColor(spell.getDisplayName()).equalsIgnoreCase(ChatColor.stripColor(name)))
			{
				return spell;
			}
		}
		
		return null;
	}
}

package net.frozenorb.magikal.spell;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.frozenorb.magikal.Profile;
import net.frozenorb.magikal.enchantments.EnchantmentManager;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public abstract class Spell {

	private Map<String, Long> lastUses = new HashMap<String, Long>();
	private Map<Profile, Boolean> activated = new HashMap<Profile, Boolean>();
	 
	public Spell() {}
	
	/**
	 * @param playerName
	 * @return the time (in seconds) since the player last activated this
	 *         ability
	 */
	public double sinceLastUse(String playerName) 
	{
		long lastUse = lastUses.containsKey(playerName) ? lastUses.get(playerName) : 0;

		return (System.currentTimeMillis() - lastUse) / 1000d;
	}

	public void setLastUse(String playerName) {
		lastUses.put(playerName, System.currentTimeMillis());
	}
	
	public boolean isActivated(Profile p) 
	{
		return activated.containsKey(p) && activated.get(p);
	}
	
	public void setActivated(Profile p, boolean activated)
	{
		this.activated.put(p, Boolean.valueOf(activated));
	}
	

	public abstract double getCooldown();
	
	public abstract int getManaCost();

	public abstract Material getMaterial();

	public abstract String getDescription();

	public abstract String getDisplayName();

	public abstract SpellSlot getSlot();

	public boolean activate(Profile profile) 
	{
		double sinceLastUse = sinceLastUse(profile.getPlayerName());
		double cooldown = getCooldown();

		if (sinceLastUse >= cooldown) 
		{
			if (getManaCost() > profile.getMana()) {
				profile.getPlayer().playSound(profile.getPlayer().getLocation(), Sound.DIG_GRASS, 1, 5);
				profile.sendMessage(ChatColor.RED + "You do not have enough mana for this!");
				return false;
			}
			else
			{
				return onActivate(profile);
			}
		}
		else 
		{
			profile.getPlayer().playSound(profile.getPlayer().getLocation(), Sound.DIG_GRASS, 1, 5);
			double cooldownLeft = cooldown - sinceLastUse;
			profile.sendMessage(ChatColor.RED + "You cannot use this for another §l" + Math.round(10.0 * cooldownLeft) / 10.0 + "§c seconds!");
			return false;
		}
	}

	/**
	 * Activates the spell for the profile
	 * 
	 * @param profile
	 * @return true, if the spell was successfully cast and should be put on
	 *         cooldown, false otherwise
	 */
	public abstract boolean onActivate(Profile profile);

	public boolean matches(ItemStack wandItem) 
	{

		ItemMeta itemMeta = wandItem.getItemMeta();

		if (itemMeta == null)
			return false;

		if (!ChatColor.stripColor(itemMeta.getDisplayName()).equalsIgnoreCase(ChatColor.stripColor(getDisplayName())))
			return false;

		return true;

	}

	public ItemStack createIcon(Profile profile) 
	{
		ItemStack icon = new ItemStack(getMaterial());
		ItemMeta meta = icon.getItemMeta();

		meta.setDisplayName("§a§l" + getDisplayName());
		
		ArrayList<String> lore = wrap(getDescription());
		
		lore.add("");
		lore.add("§6Mana Cost:§f " + getManaCost());
		lore.add("§6Cooldown:§f " + getCooldown() + " seconds");
		meta.setLore(lore);
		icon.setItemMeta(meta);

		double cooldownLeft = getCooldown() - sinceLastUse(profile.getPlayerName());
		if (cooldownLeft > 0) {
			icon.setAmount((int) cooldownLeft);
		}
		else
		{
			icon.addUnsafeEnchantment(EnchantmentManager.INVISIBLE, 1);
		}
		return icon;
	}

	public ArrayList<String> wrap(String string) 
	{
		String[] split = string.split(" ");
		string = "";
		ChatColor color = ChatColor.BLUE;
		ArrayList<String> newString = new ArrayList<String>();
		for (int i = 0; i < split.length; i++) 
		{

			if (string.length() > 20 || string.endsWith(".") || string.endsWith("!")) 
			{

				newString.add(color + string);
				if (string.endsWith(".") || string.endsWith("!")) 
				{

					newString.add("");
				}
				string = "";
			}
			string += (string.length() == 0 ? "" : " ") + split[i];
		}
		newString.add(color + string);
		return newString;
	}
}

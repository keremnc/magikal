package net.frozenorb.magikal;

import java.io.IOException;
import java.net.URL;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import net.frozenorb.magikal.bossbar.ManaUpdateTask;
import net.frozenorb.magikal.enchantments.EnchantmentManager;
import net.frozenorb.magikal.spell.Spell;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Magikal extends JavaPlugin {

	private static Magikal instance;
	
	private MagikalListener listener;
	public MagikalListener getMagikalListener() { return listener; }
	
	private ManaUpdateTask luTask;
	public ManaUpdateTask getLevelUpdateTask() { return luTask; }
	
	private ArrayList<Class<?>> spellClasses = new ArrayList<Class<?>>();
	
	private int spellAmount;
	public int getSpellAmount() { return spellAmount; }
	
	@Override
	public void onEnable() 
	{
		startLoadingSpells();
		instance = this;
		listener = new MagikalListener();
		Bukkit.getPluginManager().registerEvents(listener, this);
		loadSpells();
		
		luTask = new ManaUpdateTask();
		Bukkit.getScheduler().runTaskTimer(this, luTask, 5, 5);
		Bukkit.getScheduler().runTaskTimer(this, new Runnable() {
			private int interval;

			@Override
			public void run() 
			{
				for (Profile p : getMagikalListener().getAllProfiles().values())
				{
					
					p.getPlayer().getInventory().setHeldItemSlot(MagikalListener.DEFAULT_SLOT);
					p.updateSpellInventory();
					if (interval++ % 40 == 0) 
					{
						if (p.getMana() < p.getMaxMana())
						{
							if (p.getMana() + 10 >= p.getMaxMana())
							{
								p.setMana(p.getMaxMana());
							}
							else
							{
								p.setMana(p.getMana() + 10);
							}
						}
					}
				}
				
			}
		}, 5, 1);
		new EnchantmentManager();
	}
	
	public void startLoadingSpells() 
	{
		int counter = 0;
		
		for (Class<?> clazz : getClassesInPackage("net.frozenorb.magikal.spell.spells")) 
		{
			spellClasses.add(clazz);
			counter++;
		}
		this.spellAmount = counter;
		
	}
	
	public void loadSpells() 
	{
		int counter = 0;
		
		for (Class<?> clazz : getClassesInPackage("net.frozenorb.magikal.spell.spells")) 
		{
		
			try 
			{
				Spell spell = (Spell) clazz.newInstance();
				getMagikalListener().SPELLS[counter] = spell;
				counter++;
				System.out.println("Spell " + spell.getDisplayName() + " has been loaded.");
			} 
			catch (InstantiationException | IllegalAccessException e) 
			{
				e.printStackTrace();
			}
			
		}
	}
	
	/**
	 * Gets the running instance of the main class.
	 * @return main class
	 */
	public static Magikal getInstance() {
		return instance;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) 
	{
		if (command.getName().equalsIgnoreCase("spells"))
		{
			
			if (sender instanceof Player)
			{
				
				getMagikalListener().getProfile((Player) sender).openSpellInventory();
			}
		}
		
		return true;
	}
	/**
	 * Gets all of the classes in a package
	 * 
	 * @param pkgname
	 *            the package to get from
	 * @return list of classes
	 */
	
	public ArrayList<Class<?>> getClassesInPackage(String pkgname) 
	{
		ArrayList<Class<?>> classes = new ArrayList<Class<?>>();
		CodeSource codeSource = getClass().getProtectionDomain().getCodeSource();
		URL resource = codeSource.getLocation();
		
		String relPath = pkgname.replace('.', '/');
		String resPath = resource.getPath().replace("%20", " ");
		
		String jarPath = resPath.replaceFirst("[.]jar[!].*", ".jar").replaceFirst("file:", "");
		JarFile jFile;
		
		try 
		{
			jFile = new JarFile(jarPath);
		} 
		catch (IOException e) 
		{
			throw new RuntimeException("Unexpected IOException reading JAR File '" + jarPath + "'", e);
		}
		
		Enumeration<JarEntry> entries = jFile.entries();
		
		while (entries.hasMoreElements()) 
		{
			JarEntry entry = entries.nextElement();
			String entryName = entry.getName();
			String className = null;
			if (entryName.endsWith(".class") && entryName.startsWith(relPath) && entryName.length() > (relPath.length() + "/".length())) 
			{
				className = entryName.replace('/', '.').replace('\\', '.').replace(".class", "");
			}
			if (className != null) 
			{
				Class<?> c = null;
				try 
				{
					c = Class.forName(className);
				} 
				catch (ClassNotFoundException e) 
				{
					e.printStackTrace();
				}
				if (c != null && !c.isAnonymousClass())
					classes.add(c);
			}
		}
		try 
		{
			jFile.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}

		return classes;
	}
}
